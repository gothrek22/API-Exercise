# About

This is the answer for API Excersize from Container Solutions.

> Docker and minikube versions are incompatible, kube.sh sets minikubes docker as the default one, which stops traefik integration from working correctly.  
> Therefore Docker version should be tested completely before starting the minikube version or they should be run in different terminal sessions.

The project contains a premade .env file for use within docker-compose, the values inside can be changed to anything sane.  
For minikube deployment env vars are taken from kube secrets from within kube/secrets.yml.  

## Docker version

To run the app in docker simply run `docker-compose up`.  

After everything is up and running the app is available at http://titanic.localhost:8000/people with endpoints as requested.  

Some corners have been cut, as there is no need to overengineer security or performance in this scenario.  

## Minikube version

Everything is deployed in a custom namespace called excersize-app.  

Run the deployment via

```bash
./kube.sh
```

This should start all the important bits and open the app in your browser.