#!/bin/bash

# Wait a bit for db to init
sleep 10

# Then ask if it's available until it is
until cat < /dev/null > /dev/tcp/${DJANGO_DB_HOST}/${DJANGO_DB_PORT}; do sleep 1; done; 

# Try to make migrations and fail with custom exit code to make issue tracking easier.
./manage.py migrate || exit 129 

# Collect static files from all the modules
./manage.py collectstatic -c --noinput

# Load the example data
./manage.py initial_data --path ./titanic.csv

# For convenience sake make uwsgi serve statics. Shouldn't be done on prod.
uwsgi --http :8080 --wsgi-file api/wsgi.py --check-static /app/static