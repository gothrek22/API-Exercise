#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 <<-EOSQL
    CREATE USER ${DJANGO_DB_USER};
    CREATE DATABASE ${DJANGO_DB_NAME};
    GRANT ALL PRIVILEGES ON DATABASE ${DJANGO_DB_NAME} TO ${DJANGO_DB_USER};
    ALTER ROLE ${DJANGO_DB_USER} SET client_encoding TO 'utf8';
    ALTER ROLE ${DJANGO_DB_USER} SET default_transaction_isolation TO 'read committed';
    ALTER ROLE ${DJANGO_DB_USER} SET timezone TO 'UTC';
EOSQL