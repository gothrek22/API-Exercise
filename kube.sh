#!/bin/bash

# Reuse local docker for kubernetes
eval $(minikube docker-env)

# Build images just in case
docker-compose build

# Create namespace to nest all the deployments and pods in
kubectl apply -f ./kube/namespace.yml

# Create required secrets
kubectl apply -f ./kube/secrets.yml

# Create database as it's needed before the app starts
kubectl apply -f ./kube/db.yml

# Create the app itself so it can fill the db
kubectl apply -f ./kube/app.yml

# Create lb for db
kubectl apply -f ./kube/lb-db.yml

# Create the loadbalancer service for the app
kubectl apply -f ./kube/lb-app.yml

# Wait for deployment to finish, then open the app to make it less underwhelming
sleep 60

# Open app in browser
minikube service django -n excersize-app --format "http://{{.IP}}:{{.Port}}/people"