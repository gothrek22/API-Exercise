from django.db import models
from django.utils.translation import ugettext_lazy as _
import uuid


class Person(models.Model):
    '''Basic model for data based on csv format, most fields are nullable 
    as there was no requirement to provide all data via api POST.
    Due to lack of requirements all fields are nullable.'''
    class Meta:
        verbose_name = _("Person")
        verbose_name_plural = _("Persons")

    uuid = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    survived = models.BooleanField(blank=True, null=True)
    passengerClass = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=90, blank=True, null=True)
    sex = models.CharField(max_length=6, blank=True, null=True)
    age = models.FloatField(blank=True, null=True)
    siblingsOrSpousesAboard = models.BooleanField(blank=True, null=True)
    parentsOrChildrenAboard = models.BooleanField(blank=True, null=True)
    fare = models.FloatField(blank=True, null=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("Person_detail", kwargs={"pid": self.pid})
