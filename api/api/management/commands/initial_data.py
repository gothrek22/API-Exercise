import csv
from django.core.management import BaseCommand
from api.models import Person

class Command(BaseCommand):
    help = 'Load people csv file into the database'

    def add_arguments(self, parser):
        parser.add_argument('--path', type=str)

    def handle(self, *args, **kwargs):
        path = kwargs['path']
        with open(path, 'rt') as f:
            reader = csv.reader(f, delimiter=',')
            next(reader, None) 
            for row in reader:
                survived = True if row[0] else False
                sibling_spouse_aboard = True if row[5] else False
                parent_children_aboard = True if row[6] else False
                person = Person.objects.create(
                    survived=survived,
                    passengerClass=row[1],
                    name=row[2],
                    sex=row[3],
                    age=row[4],
                    siblingsOrSpousesAboard=sibling_spouse_aboard,
                    parentsOrChildrenAboard=parent_children_aboard,
                    fare=row[7]
                )