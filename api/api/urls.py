from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from rest_framework_swagger.views import get_swagger_view
from .views import people, person, ping
schema_view = get_swagger_view(title='Titanic API')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', schema_view)
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += [
    path('people', people),
    path('people/<uuid:uuid>', person),
    path('ping/', ping)
]
