# Split imports to stdlib, django, custom
import json

from django.shortcuts import render
from django.http import JsonResponse, HttpResponse, HttpResponseNotFound
from django_api.json_helpers import JsonResponseNotFound
from django.template import loader
from django.views.decorators.csrf import csrf_exempt


from api.models import Person


@csrf_exempt
def people(request):
    '''Unsafe way to return data from the database, shouldn't be used on production.
    Takes user uploadable data and UNSAFELY serializes it to a JsonResponse object. 
    Data should be sanitized upon upload.'''
    if request.content_type == 'application/json':
        if request.method == 'GET':
            response_json = list(Person.objects.values())
            return JsonResponse(response_json, safe=False)
        elif request.method == 'POST':
            '''Create new instance of Person model.'''
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            try:
                person = Person(
                    survived=True if body['survived'] == 'true' else False,
                    passengerClass=body['passengerClass'],
                    name=body['name'],
                    sex=body['sex'],
                    age=body['age'],
                    siblingsOrSpousesAboard=body['siblingsOrSpousesAboard'],
                    parentsOrChildrenAboard=body['parentsOrChildrenAboard'],
                    fare=body['fare']
                )
                person.save()
                data = {
                    "uuid": person.uuid,
                    "survived":  "true" if person.survived else "false",
                    "passengerClass": person.passengerClass,
                    "name": person.name,
                    "sex": person.sex,
                    "age": person.age,
                    "siblingsOrSpousesAboard": person.siblingsOrSpousesAboard,
                    "parentsOrChildrenAboard": person.parentsOrChildrenAboard,
                    "fare": person.fare
                }
                return JsonResponse(data)
            except Person.DoesNotExist as e:
                return JsonResponseNotFound({})
    else:
        people = Person.objects.values()
        template = loader.get_template('api/people.html')
        context = {
            'people': people,
        }
        return HttpResponse(template.render(context, request))


@csrf_exempt
def person(request, uuid):
    '''Same as the above, but due to format requirements, needs to be done differently. 
    Can be done differently, but it's a dirty hack to coerce data to required format.
    UUID is a primary key, so there is no chance to get more than 1 object back.'''

    if request.content_type == 'application/json':
        '''Behaviour set for json api request types.'''

        if request.method == 'GET':
            '''Return a valid json object for specific uuid that has been requested via context path.
            Or return a 404 error. UUID format is validated on framework level'''
            try:
                person = Person.objects.get(uuid=uuid)
                data = {
                    "uuid": person.uuid,
                    "survived":  "true" if person.survived else "false",
                    "passengerClass": person.passengerClass,
                    "name": person.name,
                    "sex": person.sex,
                    "age": person.age,
                    "siblingsOrSpousesAboard": person.siblingsOrSpousesAboard,
                    "parentsOrChildrenAboard": person.parentsOrChildrenAboard,
                    "fare": person.fare
                }
                return JsonResponse(data)
            except Person.DoesNotExist as e:
                return JsonResponseNotFound({})

        elif request.method == 'DELETE':
            '''Delete specific object from the Person entity using provided uuid as key.
            UUID is validated as part of framework. 
            If uuid doesn't exist return 404 with correct content type. '''
            try:
                person = Person.objects.filter(uuid=uuid).delete()
                return JsonResponse({})
            except Person.DoesNotExist as e:
                return JsonResponseNotFound({})

        elif request.method == 'PUT':
            '''Update existing Person model instances or return 404 if no such person exists.'''
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            person = Person.objects.get(uuid=uuid)
            try:
                for key in body.keys():
                    if body[key]:
                        value = body[key]
                        if value == 'true':
                            value = True
                        elif value == 'false':
                            value = False
                        setattr(person, key, value)
                person.save()
                return JsonResponse({})
            except Person.DoesNotExist as e:
                return JsonResponseNotFound({})

    else:
        if request.method == 'GET':
            try:
                person = Person.objects.get(uuid=uuid)
                template = loader.get_template('api/person.html')
                context = {
                    'person': person,
                }
                return HttpResponse(template.render(context, request))
            except Person.DoesNotExist as e:
                return HttpResponseNotFound('')


def ping(request):
    return HttpResponse('PONG')